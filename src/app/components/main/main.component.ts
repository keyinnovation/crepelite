import { Component, OnInit, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { JuegosComponent } from '../juegos/juegos.component';
import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild('asiento1', {read: ViewContainerRef}) containerAsiento1:any;
  @ViewChild('asiento2', {read: ViewContainerRef}) containerAsiento2:any;
  @ViewChild('asiento3', {read: ViewContainerRef}) containerAsiento3:any;
  @ViewChild('asiento4', {read: ViewContainerRef}) containerAsiento4:any;
  @ViewChild('asiento5', {read: ViewContainerRef}) containerAsiento5:any;
  @ViewChild('asiento6', {read: ViewContainerRef}) containerAsiento6:any;
  containers = [];

  constructor(private resolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.containers = [this.containerAsiento1, this.containerAsiento2, this.containerAsiento3, this.containerAsiento4, this.containerAsiento5, this.containerAsiento6];
  }

  ngAfterContentInit() {
    for (let i = 0; i < 6; i++) {
        this.cambiarPantalla(this.containers[i], MenuComponent, {asiento: {asiento: i+1}});
    }
  }

  cambiarPantalla( asiento:any, componente:any, params:any = {} ) {

      if(asiento == null){
        return;
      }
      asiento.clear();
      const factory = this.resolver.resolveComponentFactory(componente);
      var componenteFinal = asiento.createComponent(factory);
      for(var objeto in params){
        componenteFinal.instance[objeto] = params[objeto];
      }
      if(componente != MenuComponent){
        var back = document.getElementById('#back'+asiento);
        back.style.display = '';
      }

  }

  irAtras(numAsiento:any){
    console.log("Estamos entrando a la funcion ir atras");
    var back = document.getElementById('#back'+numAsiento);
    back.style.display = 'none';
    this.cambiarPantalla(this.containers[numAsiento - 1], MenuComponent, { asiento: [numAsiento - 1] });
  }

}
