import { Component, OnInit, Input } from '@angular/core';
import { UnityService } from '../../modules/unity-linker/services/unity.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  @Input() juego:any;
  @Input() asiento = 0;

  gameInstance = null;

  src = './assets/farmfox_html/Build/farmfox_html.json';

  constructor(private _unityService: UnityService) {
    
  }

  ngOnInit() {
    // Buscar id de juego correspondiente a esta instancia
    
  }

  ngAfterViewInit() {
    this.gameInstance = this._unityService.load(this.getGameId(), this.src);
  
    window["addGameInstanceRef"](
      // Objeto con referencia de instancia de juego y número de instancia
      {
        instance: this.gameInstance,
        asiento: this.asiento,
        initialized: false
      }
    );
  }

  getGameId () {
    return 'game-container-' + this.asiento;
  }
}
