import { Component, OnInit } from '@angular/core';
import 'hammerjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  asiento:any;
  mostrarJuego:boolean = false;
  juegoSeleccionado:any;

  constructor() { }

  ngOnInit() {
  }

  jugar(){
    console.log("Jugar");
    console.log(this.asiento.asiento);
    this.mostrarJuego = true;
  }

  cerrarJuego() {
    this.mostrarJuego = false;
  }

}
