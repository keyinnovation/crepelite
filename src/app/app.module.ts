import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { AppRoutingModule } from './app-routing.module';
import { ROUTES } from './app.routes';
import { UnityLinkerModule } from './modules/unity-linker/unity-linker.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { JuegosComponent } from './components/juegos/juegos.component';
import { MenuComponent } from './components/menu/menu.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    JuegosComponent,
    MenuComponent
  ],
  imports: [
	UnityLinkerModule,
    BrowserModule,
    // AppRoutingModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
  ],
  entryComponents: [
    MenuComponent,
    JuegosComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
