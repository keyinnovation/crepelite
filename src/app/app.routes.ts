
import { Routes } from '@angular/router';
import {MainComponent} from './components/main/main.component';


export const ROUTES:Routes = [
  {path: 'main', component:  MainComponent},
  {path: '', pathMatch: 'full', redirectTo: 'main'},
  {path: '**', pathMatch: 'full', redirectTo: 'main'}
];
