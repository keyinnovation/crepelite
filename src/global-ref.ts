export interface MyGlobal {
    communicateGameInstancesOrientation: number;
}

export abstract class GlobalRef {
    abstract get nativeGlobal(): MyGlobal;
}